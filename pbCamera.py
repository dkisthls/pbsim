### Library imports ###

import copy                  # For cloning settings dictionary
import logging, sys          # For log generation
import numpy as np           # For generating image array for fits file
import select                # For dealing with non-blocking sockets with a timeout (stop cmd)
import shutil                # For copying fits files
import socket                # For dealing with all things sockets
import threading
import time                  # For generating timing and sleep calls

from astropy.io import fits  # For saving fits data
from datetime import datetime, timedelta
from pathlib import Path     # For dealing with sim data paths/filenames
from multiprocessing import Process, Queue, current_process, freeze_support

### PizzaBox Sim Imports ###
from pbDefined import pbCommands as pbcmd
from pbDefined import pbConstants as const
from pbDefined import pbDefaults as default
from pbDefined import pbKeys as keys

# Class that encapsulates a single readout of a readout sequence from the camera
# where:
#  target_time: The time from the start of the exposure cycyel to when the defined
#               action is to take place and is inclusive of:
#                 SFRAME/FFRAME
#                   - Single reset/read frame read time -or-
#                   - Single frame read time + exposure dt (interframe gap) -or-
#                   - Frame read time + exposure dt + T_USB (last frame only)
#                 RSTRDRD
#                   - Time for complete exposure + 4 frame reads + T_USB
#       action: The action to be performed following target_time. Valid actions
#               are:
#                 NO_ACTION: Do nothing after target time is reached.
#                 WRITE_FILE: Write the next file to the output.
#    frame_num: The frame number within the context of this readout sequence.
#               This is analogous to NDR number for SFRAME/FFRAME or one of the
#               four frames for RSTRDRD.
#   frame_type: A short description which can be: 'BIAS', 'RESET', 'READ'
#
class Readout():
    def __init__(self, target_time, action, frame_num, frame_type ):
        self.target_time = target_time
        self.action = action
        self.frame_num = frame_num
        self.frame_type = frame_type

    def __str__(self):
        return str(
            'targetTime={:.03f}, action={:10}, frameNum={}, frameType={}'.format(
            self.target_time, self.action, self.frame_num, self.frame_type))
#
# End of class Readout


# Simple class that encapsulates the source/target, logging info, and
# response string for a single readout when the source is a FrameGen FITS HDU.
#   hdu: A FraemGen FITS HDU created on the fly.
#   tgt_filename: The output path/filename that the HDU will be written to.
#   log_info: Additional logging info for the FileWriter.
#   resp_str: The 'WRITTEN' response following the write to tgt_filename.
class FrameGenHdu:
    def __init__(self, hdu, tgt_filename, extra_log_info, resp_str=None):
        self.hdu = copy.deepcopy(hdu)
        self.tgt_filename = copy.deepcopy(tgt_filename)
        self.extra_log_info = copy.deepcopy(extra_log_info)
        self.resp_str = copy.deepcopy(resp_str)

    def set_written_response(self, resp_str):
        self.resp_str = copy.deepcopy(resp_str)
#
# End of class FrameGenHdu


# Simple class that encapsulates the source/target, logging info, and
# response string for a readout when the source is a FITS sim data file.
#   src_filename: The file that data will be copied from.
#   tgt_filename: The output path/filename that src_filename will be copied to.
#   log_info: Additional logging info for the FileWriter.
#   resp_str: The 'WRITTEN' response following the write to tgt_filename.
class SimFile:
    def __init__(self, src_filename, tgt_filename, extra_log_info, resp_str=None):
        self.src_filename = copy.deepcopy(src_filename)
        self.tgt_filename = copy.deepcopy(tgt_filename)
        self.extra_log_info = copy.deepcopy(extra_log_info)
        self.resp_str = copy.deepcopy(resp_str)

    def set_written_response(self, resp_str):
        self.resp_str = copy.deepcopy(resp_str)
#
# End of class SimFile


# Basic class that tries to mimic the relevant internal states of PBserver
# FRAME modes
# In FRAME modes, number of NDRs and their relationship to the exposure depends on
# the mode:
#   SFRAME: nNDR = Bias + N Science Samples
#                  It accounts for the number of frames returned but not for all frames
#                  read. The frame order is:
#             Reset Frame: (must be accounted for in timing but it is NOT part of
#                           nNDR and it is not returned)
#              Bias Frame: Exposure starts at beginning of BIAS readout.
#                          Occupies 1 frame read time.
#                  nNDR-1: Science frame(s). Science frames are equally spaced to cover
#                          the exposure time with the final science frame read time starting
#                          at end of the exposure time.
#
#   FFRAME: nNDR = Reset + Bias + N Science Samples
#             Reset Frame: Preceeds Bias Frame and is returned.
#                          Occupies 1 frame read time
#              Bias Frame: Exposure starts at beginning of BIAS readout.
#                          Occupies 1 frame read time.
#                  nNDR-2: Science frame(s). Science frames are equally spaced to cover
#                          the exposure time with the final science frame read time starting
#                          at end of the exposure time.
#
# 1 NDR: Minimum number for SFRAME (Reset(but not returned) + Bias + 1 Science
#        NOTE: There is also a 'reset' frame that is read and not returned. It
#              must be accounted for in timing but is not part of NDR.
# 2 NDR: Minimum number for FFRAME (Reset + Bias + 1 Science)
# Values of NDR that are > minimum imply additional science frames evenly distributed across
# the defined exposure time.

class PBCamera:
    # PBCamera class variables
    lk_info = threading.Lock()
    info = {
        'ndr_count': 0,
        'coadd_count': 0,
        'controller_go': 0,
        'readout_count': 0,
        'written_count': 0,
        'status': 0,
        'total_reads': 0,
        'init_count': 0
    }

    # 'set' command values
    lk_settings = threading.Lock()
    settings = {
        keys.S_DT:    default.DT_S,        # in seconds
        keys.S_MODE:  default.MODE,
        keys.S_H:     const.SENSOR_HEIGHT,
        keys.S_Y:     default.SUBARRAY_START_ROW,
        keys.S_NNDR:  const.MIN_NDRS[default.MODE],
        keys.S_TRIG:  default.TRIGGER,

        # File naming
        keys.S_PATH:  default.PATH_TO_TMPFS,
        keys.S_FNAME: default.FILENAME,
        keys.S_FNUM:  default.FILE_NUMBER,
    }

    # 'data' command FrameGen/Sim values
    lk_data = threading.Lock()
    data = {
        keys.D_FRAMEGENMODE:    default.FRAMEGEN_MODE,
        keys.D_FIXEDPIXELVALUE: default.FIXED_PIXEL_VALUE,
        keys.D_PATH:            "",
        keys.D_BASEFILENAME:    "",
        keys.D_FILESPERSET:     0,
        keys.D_FRAMECOUNT:      0,
        keys.D_SRCFILEINDEX:    0
    }

    hdu = None           # FITS Header Data Unit(file) created by the framegen

    lk_exp_in_progress = threading.Lock()
    exp_in_progress = 0

    # PBCamera ctor
    #---------------------------- __init__() -----------------------------------
    # cmdLineArgs - The command line argumentspbcam - The pbCamera object
    # socket_handler - cmd/resp socket
    # file_handler - Responsible for writing files to disk
    def __init__(self, cmdLineArgs, socket_handler, file_handler):
        self._file_handler = file_handler      # For queue files to be written
        self._socket_handler = socket_handler  # Command/response interface socket

        # Indicates array as been powered-on (initialized) via the 'init' command
        self._initialized = False

        # Circular(ramp) sequence counter. Used in conjunctino with file name and
        # file number from settings to create a complete filename of the form:
        #   fname-seqNum-frameNum
        self.seqNumber = default.SEQ_NUMBER

        # Save timing values from command line argument parser.
        # All cmdline arguments were initially set to defaults from pbDefaults.
        # Defaults may have been overridden by values supplied on the command line.
        self.t_latTimes = copy.deepcopy(cmdLineArgs.lat)
        self.t_rendTimes = copy.deepcopy(cmdLineArgs.rend)
        self.t_lineTimes = copy.deepcopy(cmdLineArgs.lt)
        self.t_fsyncTimes = copy.deepcopy(cmdLineArgs.fsync)
        self.t_vclkTimes = copy.deepcopy(cmdLineArgs.vclk)
        self.t_frst = cmdLineArgs.frst
        self.t_hrst = cmdLineArgs.hrst
        self.t_sdel = cmdLineArgs.sdel
        self.t_lrst = cmdLineArgs.lrst
        self.t_lfend = cmdLineArgs.lfend
        self.t_crt = cmdLineArgs.crt

        # PBCamera locks and condition vars
        self._lk_stop = threading.Lock()      # For use with a 'stop' command
        self._cv_stop = threading.Condition(self._lk_stop)
        self._lk_exposure = threading.Lock()  # For use when starting/stopping
                                              # an exposure

        # Create an initial image array
        PBCamera.make_image(
            PBCamera.data[keys.D_FRAMEGENMODE],
            PBCamera.settings[keys.S_H],
            PBCamera.data[keys.D_FRAMECOUNT])
    #
    # End of PBCamera.__init__()


    #------------------------- _build_filename() -------------------------------
    #
    def _build_filename(base_filename, idx, ext):
        filename = base_filename + \
                   ("." if base_filename.endswith('.') == False else "") + \
                   str(idx).zfill(3) + "." + ext
        return filename
    #
    # End of PBCamera._build_filename()


    #--------------------------- make_image() ----------------------------------
    #
    # Function for generating a test image array matrix
    #   frame_gen_mode: One of 'ColumnCount', 'FixedPixel', 'FrameCount', '"RowCount'
    #                h: The current h(number of rows setting for the array
    #      frame_count: The current frame count value and is only used if
    #                   frame_gen_mode == 'FrameCount'
    def make_image(frame_gen_mode, h, frame_count=None):
        w = const.SENSOR_WIDTH

        # Fixed Pixel Value
        if 'FixedPixel' == frame_gen_mode:
            # All pixels contain the same value
            im = np.full((w, h), PBCamera.data[keys.D_FIXEDPIXELVALUE], dtype=const.BIT_TYPE)

        elif 'FrameCount' == frame_gen_mode:
            # All pixels contain the current frame_count value
            im = np.full((w, h), frame_count, dtype=const.BIT_TYPE)

        elif 'ColumnCount' == frame_gen_mode:
            # Same as RowCount(i.e. original) but with Fortran ordering (column ordered)
            # All pixels in a column contain the column number
            im = np.arange(h * w, dtype=const.BIT_TYPE).reshape(h, w, order='F')
        else:
            # The original image generator with default C ordering (row ordered)
            # All pixels in a row contain the row number
            im = np.arange(h * w, dtype=const.BIT_TYPE).reshape(h, w)

        PBCamera.hdu = fits.PrimaryHDU(im)
    #
    # End of PBCamera.make_image()


    #---------------------- _create_readout_list() -----------------------------
    #
    # trigger_time is a datetime object
    # All timing params are in seconds
    def _create_readout_list(self,
        settings, trigger_time, t_start, t_frame_read, t_last_frame_extra):

        # Retrieve relevant settings used multiple times once here
        mode = settings[keys.S_MODE]
        t_dt = settings[keys.S_DT]
        num_output_frames = settings[keys.S_NNDR]

        # Create list of readouts to simulate resets, frame reads, including
        # inter-frame gap times, for each of the NDRs in this exposure.
        list_of_readouts = []

        target_time = trigger_time    # Initialize with trigger time

        # First frame
        # time_to_wait(SFRAME)  = reset + 1 frameRead
        # time_to_wait(FFRAME)  = reset
        # time_to_wait(RSTRDRD) = reset + 1 frameRead + lastFrameExtra == total wait
        frame_num = 1
        if 'SFRAME' == mode:
            # For SFRAME wait the amount of time for the reset plus one frame
            # read time for the bias frame.
            # At the end of this wait-time:
            #  - The reset frame has been read but will not be delivered
            time_to_wait = t_start                      # tlat + Reset frame
            target_time += time_to_wait
            list_of_readouts.append(Readout(target_time, 'NO_ACTION', 0, 'RESET'))

            # At the end of this wait-time:
            #  - The bias frame has been read and ready to be delivered
            time_to_wait = t_frame_read                 # Bias frame
            frame_type = 'BIAS'

        elif 'FFRAME' == mode:
            # For FFRAME wait the amount of time for the reset frame.
            # At the end of this wait-time:
            #  - The reset frame has been read and ready to delivered
            time_to_wait = t_start                      # tlat + Reset frame
            frame_type = 'RESET'

        else:  # 'RSTRDRD' == mode
            num_output_frames *= const.RSTRDRD_FRAMES_PER_EXP

            # For RSTRDRD we will wait the entire ramp time
            # In the subsequent for loop we'll just have N back-to-back writes.
            #   FRAME:  RESET        SCI
            time_to_wait = t_start + t_frame_read + t_last_frame_extra
            frame_type = 'SCI'

        target_time += time_to_wait

        list_of_readouts.append(
            Readout(target_time, 'WRITE_FILE', frame_num, frame_type))

        #  NOTE: nOutputFrame
        #  SFRAME: frame_num == 0       : reset frame time consumed but
        #                                 frame is not queued
        #          frame_num == 1       : bias frame just queued
        #                                 wait for remaining frame exp dt
        #          1 < frame_num < nNDR : science frame just queued,
        #                                 wait for remaining frame exp dt
        #          frame_num == nNDR    : final science frame just queued,
        #                                 acquistion cycle is complete
        #
        #  FFRAME: frame_num == 1       : reset frame just queued,
        #                                 wait for remaining frame exp dt
        #          frame_num == 2       : bias frame just queued,
        #                                 wait for remaining frame exp time
        #          2 < frame_num < nNDR : science frame just queued,
        #                                 wait for remaining frame exp time
        #          frame_num == nNDR    : final science frame just queued,
        #                                 acquisiton cycle is complete
        for frame_num in range(2, num_output_frames + 1):
            frame_type = 'SCI'
            time_to_wait = 0.0

            # For SFRAME and FFRAME wait for the remaining frame read time.
            # For RSTRDRD the entire reset + frame read + extra was waited on
            # prior to this for loop so no additional wait.
            if mode in ('SFRAME', 'FFRAME'):
                time_to_wait = t_dt + t_frame_read
                if 'FFRAME' == mode and frame_num == 2:
                    frame_type = 'BIAS'

                if frame_num == num_output_frames:
                    time_to_wait += t_last_frame_extra

            target_time += time_to_wait
            list_of_readouts.append(
                Readout(target_time, 'WRITE_FILE', frame_num, frame_type))

        return (list_of_readouts, num_output_frames)
    #
    # End of PBCamera._create_readout_list()


    #------------------------- _figure_timing() --------------------------------
    # Function for calculating the timing values for the components that
    # comprise a readout sequence (either SFRAME, FFRAME, or RSTRDRD).
    # Returns: (t_start, t_frame_read, t_last_frame_extra, t_total_ramp_time):
    #    t_start            - Time from trigger to completion of reset
    #    t_frame_read       - Time to read one frame
    #    t_last_frame_extra - Additioinal time following last frame
    #    t_total_ramp_time  - Total time of the ramp from reset to last frame read
    # NOTE: All times during calculations are in nanoseconds.
    #       Return values are converted to seconds
    def _figure_timing(self, settings):
        t_start = t_frame_read = t_last_frame_extra = t_total_ramp_time = 0.0

        # Retrieve relevant settings used multiple times once here
        mode = settings[keys.S_MODE]
        nNDR = settings[keys.S_NNDR]
        y = settings[keys.S_Y]
        h = settings[keys.S_H]
        dt_ns = settings[keys.S_DT] * const.NS_PER_SECOND     # in nano-seconds

        # Set line time, fsync, and vclk based on selected mode
        if 'SFRAME' == mode:
            t_lat = self.t_latTimes[keys.SLOW]
            t_rend = self.t_rendTimes[keys.SLOW]
            t_lt = self.t_lineTimes[keys.SLOW]
            t_fsync = self.t_fsyncTimes[keys.SLOW]
            t_vclk = self.t_vclkTimes[keys.SLOW]
        else:
            if 'FFRAME' == mode:
                t_lat = self.t_latTimes[keys.FAST]
                t_rend = self.t_rendTimes[keys.FAST]
            else:
                t_lat = self.t_latTimes[keys.RSTRDRD]
                t_rend = self.t_rendTimes[keys.RSTRDRD]

            t_lt = self.t_lineTimes[keys.FAST]
            t_fsync = self.t_fsyncTimes[keys.FAST]
            t_vclk = self.t_vclkTimes[keys.FAST]

        if 'SFRAME' == mode:           # SlowUpTheRamp
            t_start = t_lat + self.t_frst
            t_frame_read = t_fsync + (y * t_vclk) + (h * t_lt)
            t_last_frame_extra = t_rend

            t_total_ramp_time = t_lat + \
                                self.t_frst + \
                                (2 * t_frame_read) + \
                                ((nNDR - 1) * (t_frame_read + dt_ns)) + \
                                t_rend

        elif 'FFRAME' == mode:         # FastUpTheRamp
            t_start = t_lat + self.t_frst + self.t_sdel
            t_frame_read = t_fsync + (y * t_vclk) + ((h + 1) * t_lt)
            t_last_frame_extra = t_rend

            t_total_ramp_time = t_lat + \
                                self.t_frst + \
                                self.t_sdel + \
                                (nNDR * t_frame_read) + \
                                ((nNDR - 1) * dt_ns) + \
                                t_rend

        elif 'RSTRDRD' == mode:        # LineByLine
            t_4lines = self.t_lrst + \
                       (4 * t_lt) + \
                       (3 * self.t_hrst) - \
                       (3 * t_vclk) + dt_ns

            t_start = t_lat + self.t_frst
            t_frame_read = t_fsync + (y * t_vclk) + (h * t_4lines) + self.t_lfend
            t_last_frame_extra = t_rend

            t_total_ramp_time = t_lat + \
                                self.t_frst + \
                                t_frame_read + \
                                t_rend

        else:
            # If a new mode is ever added this will flag that code needs to
            # be added in this method to handle it
            err_msg = 'Unhandled readout mode {} in PBCamera._figure_timing()!'.format(mode)
            logging.info(err_msg)
            raise RuntimeError(err_msg)

        return (
            t_start / const.NS_PER_SECOND,
            t_frame_read  / const.NS_PER_SECOND,
            t_last_frame_extra / const.NS_PER_SECOND,
            t_total_ramp_time / const.NS_PER_SECOND)
    #
    # End of PBCamera._figure_timing()


    #------------------------ get_next_frame_no() ------------------------------
    # Get next file number and circularly increment
    # Return value is in the range: 0 <= outNum <= MAX_FILES - 1
    def get_next_frame_no():
        outNum = PBCamera.settings[keys.S_FNUM]
        PBCamera.settings[keys.S_FNUM] = (PBCamera.settings[keys.S_FNUM] + 1) % const.MAX_FILES
        return outNum
    #
    # End of PBCamera.get_next_frame_no()


    #------------------------- get_next_seq_no() -------------------------------
    # Get next sequence number and circularly increment.
    # Return value is in the range: 0 <= outNum <= MAX_SEQUENCES - 1
    def get_next_seq_no(self):
        outNum = self.seqNumber
        self.seqNumber = (self.seqNumber + 1) % const.MAX_SEQUENCES
        return outNum
    #
    # End of PBCamera.get_next_seq_no()


    #------------------------ execute_exposure() -------------------------------
    #
    def execute_exposure(self, trigger_time):
        # Snapshot current settings and data before proceeding.
        settings = self.get_settings()
        data = self.get_data()

        # Initialize some local vars
        seq_num = self.get_next_seq_no()

        (t_start,                   # trigger through reset
            t_frame_read,           # 1 frame read time
            t_last_frame_extra,     # extra added following last frame read
            t_total_ramp_time) = self._figure_timing(settings)

        (list_of_readouts, num_output_frames) = self._create_readout_list(
            settings,
            trigger_time, t_start, t_frame_read, t_last_frame_extra)

        # Reset some info parameters for this exposure
        with PBCamera.lk_info:
            PBCamera.info['ndr_count'] = num_output_frames
            PBCamera.info['readout_count'] = 0
            PBCamera.info['written_count'] = 0
            PBCamera.info['total_reads'] = num_output_frames

        # Create an Event to pass to the readout sequence thread. It will signal
        # this event when the sequence completes.
        ev_exposure_complete = threading.Event()
        ev_exposure_complete.clear()

        readout_sequence = PBCamera.ReadoutSequence(
            settings,               # Current 'set' cmd values
            data,                   # Current 'data' cmd values
            seq_num,                # Current readout sequence number
            list_of_readouts,       # List of target times/actions for readouts
            self._file_handler,     # For writing frames
            self._cv_stop,          # For stopping an active readout sequence
            ev_exposure_complete)

        with self._lk_exposure:
            readout_sequence.start()

        # Return an event and timeout value the caller can use to wait for
        # this exposure to complete.
        return (ev_exposure_complete, t_total_ramp_time)
    #
    # End of PBCamera.execute_exposure()


    # Sets initialized to True after a wait time
    def init(self):
        logging.info("Initializing array...")
        time.sleep(const.INIT_TIME)
        self._initialized = True

    # pof command, similar to above.  Maybe more sophistacted than the real thing.
    def pof(self):
        if self._initialized:
            logging.info("Powering off array...")
            self._initialized = False
        else:
            logging.info("Array already powered off, doing nothing")

    def stop(self):
        # Grab lock to prevent new exposure starting while we issue a stop.
        with self._lk_exposure:
            with self._lk_stop:
                self._cv_stop.notify_all()


    # ------------------------ is_initialized() --------------------------------
    #
    # Utility method used to check whether the camera/array is initialized
    # (i.e. has received an 'init' command) or if it is unitialized
    # (i.e. has never received an 'init' or has received a 'pof' which
    # powers-off/uninitializes the camera/array).
    def is_initialized(self):
        return self._initialized

    def get_num_exp_in_progress(self):
        with PBCamera.lk_exp_in_progress:
            return PBCamera.exp_in_progress

    ### 'info' sub-commands ###
    def get_info(self):
        with PBCamera.lk_info:
            return copy.deepcopy(PBCamera.info)

    def get_info_value(self, name):
        with PBCamera.lk_info:
            return PBCamera.info[name]

    def inc_info_value(self, name):
        with PBCamera.lk_info:
            PBCamera.info[name] += 1

    def set_info_value(self, name, value):
        with PBCamera.lk_info:
            PBCamera.info[name] = value

    ### 'set' sub-commands ###
    def get_settings(self):
        with PBCamera.lk_settings:
            return copy.deepcopy(PBCamera.settings)

    def update_settings(self, newSettings):
        with PBCamera.lk_settings:
            # Merge new settings with existing settings and save back
            PBCamera.settings = {**PBCamera.settings, **newSettings}

    ### 'data' sub-command ###
    def get_data(self):
        with PBCamera.lk_data:
            return copy.deepcopy(PBCamera.data)

    def update_data(self, new_data):
        makeImage = False
        with PBCamera.lk_data:
            frame_gen_mode = PBCamera.data[keys.D_FRAMEGENMODE]  # init to curr value

            # Look for a new mode
            if keys.D_FRAMEGENMODE in new_data:
                # New frame gen mode detected, save new mode for later use
                frame_gen_mode = new_data[keys.D_FRAMEGENMODE]

                # Is it a mode that needs a static image to be generated?
                if frame_gen_mode not in ('Off', 'FrameCount'):

                    # Yes it is. If the mode is changing, make a new image.
                    if not frame_gen_mode == PBCamera.data[keys.D_FRAMEGENMODE]:
                        makeImage = True

                    # Yes, but the new mode is the same as the current mode. Make a
                    # new image if the mode is 'FixedPixelValue' and there's a new value
                    elif 'FixedPixel' == frame_gen_mode and\
                         keys.D_FIXEDPIXELVALUE in new_data and\
                         new_data[keys.D_FIXEDPIXELVALUE] != PBCamera.data[keys.D_FIXEDPIXELVALUE]:
                        makeImage = True

            # No mode change but, if the current mode is "FixedPixel" and there's
            # a new value, make a new image.
            elif 'FxedPixel' == frame_gen_mode and\
                 keys.D_FIXEDPIXELVALUE in new_data and\
                 new_data[keys.D_FIXEDPIXELVALUE] != PBCamera.data[keys.D_FIXEDPIXELVALUE]:
                makeImage = True

            # Reset file index for any change to filepath/name arguments
            if keys.D_PATH in new_data or\
               keys.D_BASEFILENAME in new_data or\
               keys.D_FILESPERSET in new_data:
                PBCamera.data[keys.D_SRCFILEINDEX] = int(0)    # reset current source file index

            # Merge new settings with existing settings and save back
            PBCamera.data = {**PBCamera.data, **new_data}

            if makeImage:
                PBCamera.make_image(frame_gen_mode, PBCamera.settings[keys.S_H])

    # For supplying the response string to an 'info' command:
    # INFO
    #   ndr_count:     This is the rolling count that is decremented as data
    #                  comes in for a coad cycle.
    #   coadd_count:   This is the rolling count that is incremented as coad
    #                  cycles are completed.
    #   controller_go: Status bit indicating whether the system is still
    #                  working on or waiting for data.
    #   readout_count: This is a rolling count of how many total readouts
    #                  have been done.
    #   written_count: This is a rolling count of the number of files that
    #                  have been written.
    #   status:        Status bit indicating what the system system is
    #                  doing.
    #   total_reads:   Total number of readouts expected for the currently
    #                  set operation.
    #   init_count:    Number of times the init command has completed.
    # SET
    #   filename:      Base filename to be used for files written.
    #   filenumber:    Base filenumber to be used in the final filename for
    #                  files written.
    #   path:          Directory where files are set to be written to.
    #   ndr_count:     Total number of frames per coad cycle expected based on
    #                  current configuration.
    #   coadd_count:   Total number of coad cycles expected based on current
    #                  configuration.
    #   readout_mode:  Configured readout mode
    #                  (SFRAME, FFRAME, RSTRDRD)
    #   yyy:           Number of rows to skip.
    #   rows:          Number of rows to read.
    #   trigger_count: Number of times trigger has been received.
    #   trigger_flag:  0 if not in trigger mode, 1 if in trigger mode.
    #   post_readout_delay_reg: Register value for post_readout_delay
    #                                  (20ns clock).
    #   post_readout_delay_ns:  Number of nanoseconds to wait after a
    #                           readout.
    #   pcopf:         pixel_clock_osc_pulses_fast, the number of 20ns cycles
    #                  given to a pixel for fast mode.
    #   pcoplbl:       pixel_clock_osc_pulses_lbl, the number of 20ns cycles
    #                  given to a pixel for lbl mode.
    def __repr__(self):
        return str("""      ndr_count: %d
      coadd_count: 0
      controller_go: %d
      readout_count: %d
      written_count: %d
      status: 1
      total_reads : %d
      init_count: %d
SET
      filename: %s
      filenumber: %d
      path: %s
      ndr_count: %d
      coadd_count: 1
      readout_mode: %s
      yyy: %d
      rows: %d
      trigger_flag: %d
      trigger_count: 0
      post_readout_delay_reg: 0
      post_readout_delay_ns: 0
      pcopf: 28
      pcoplbl: 28
      usb_serial_number: simulated""" % (
            PBCamera.info['ndr_count'],
            PBCamera.info['controller_go'],
            PBCamera.info['readout_count'],
            PBCamera.info['written_count'],
            PBCamera.info['total_reads'],
            PBCamera.info['init_count'],
            PBCamera.settings[keys.S_FNAME],
            PBCamera.settings[keys.S_FNUM],
            PBCamera.settings[keys.S_PATH],
            PBCamera.settings[keys.S_NNDR],
            PBCamera.settings[keys.S_MODE],
            PBCamera.settings[keys.S_Y],
            PBCamera.settings[keys.S_H],
            PBCamera.settings[keys.S_TRIG]))


    class ReadoutSequence(threading.Thread):
        #------------------------ __init__() -----------------------------------
        #
        #         settings: Deepcopy of current 'set' command dict.
        #             data: Deepcopy of current 'data' command dict.
        #     sequence_num: The current sequence number for use in all
        #                   file names associated with this readout sequence.
        # list_of_readouts: The list of target times and actions to take for
        #                   frame in this readout sequence.
        #     file_handler: The FileHandler object used for file/hdu writes
        #          cv_stop: Condition variable for stopping the sequence
        #      ev_complete: Event for signaling completion of the sequence
        def __init__(self,
            settings, data, sequence_num, list_of_readouts, file_handler,
            cv_stop, ev_complete):
            threading.Thread.__init__(self)

            self._settings = settings    # Curent 'set' cmd values
            self._data = data            # Current 'data' cmd values
            self._mode = self._settings[keys.S_MODE]
            self._sequence_num = sequence_num
            self._frame_num = 0          # Current frame in readout sequence
            self._list_of_readouts = copy.deepcopy(list_of_readouts)
            self._file_handler = file_handler
            self._cv_stop = cv_stop
            self._ev_complete = ev_complete

            self._num_output_frames = len(self._list_of_readouts)
            self._frames_to_write = []   # A list/queue for preped files/hdu's

            # Set the total number of frames for this readout sequence
            self._num_output_frames = len(self._list_of_readouts)
            if 'SFRAME' == self._mode:
                self._num_output_frames -= 1

            # Build up extra logging info string
            self._frame_gen_info_str = ''
            if not 'Off' == self._data[keys.D_FRAMEGENMODE]:
                if 'FixedPixel' == self._data[keys.D_FRAMEGENMODE]:
                    self._frame_gen_info_str = "{}={}".format(
                        self._data[keys.D_FRAMEGENMODE],
                        self._data[keys.D_FIXEDPIXELVALUE])

                elif not 'FrameCount' == self._data[keys.D_FRAMEGENMODE]:
                    # 'RowCount' or 'ColumnCount'
                    self._frame_gen_info_str = self._data[keys.D_FRAMEGENMODE]

            # If this is a RSTRDRD then ready all 4 frames to be used following
            # the entire rstrdrd expsoure time. Plenty of time to do this here!
            # They will be queued for write at the end of the exposure...
            start = time.time()
            if 'RSTRDRD' == self._mode:
                # Copy current framecount value in case frameGenMode='FrameCount'
                # We need unique values but we won't actually increment the data
                # value until the write occurs during ReadoutSequence.run().
                curr_frame_count = self._data[keys.D_FRAMECOUNT]

                for i in range(self._num_output_frames):
                    frame_info = self.__prep_frame_data(
                        self._data[keys.D_FRAMEGENMODE], curr_frame_count)

                    self._frames_to_write.append(frame_info)
                    curr_frame_count += 1

        #
        # End of ReadoutSequence.__init__()


        #------------------ __get_next_frame_num() -----------------------------
        # Get next file number and circularly increment
        # Return value is in the range: 0 <= outNum <= MAX_FILES - 1
        def __get_next_frame_num(self):
            frame_num = self._frame_num
            self._frame_num = (self._frame_num + 1) % const.MAX_FILES
            return frame_num
        #
        # End of ReadoutSequence.__get_next_frame_num()


        #-------------------- __prep_frame_data() ------------------------------
        #
        def __prep_frame_data(self, frame_gen_mode, frame_count=None):
            frame_num = self.__get_next_frame_num()

            tgt_filename = '{}{}-{:05d}-{:05d}.fits'.format(
                self._settings[keys.S_PATH], self._settings[keys.S_FNAME],
                self._sequence_num, frame_num )

            if 'Off' == frame_gen_mode:
                idx = int(self._data[keys.D_SRCFILEINDEX])
                src_filename = Path(self._data[keys.D_PATH]) / \
                    PBCamera._build_filename(
                        self._data[keys.D_BASEFILENAME], idx, 'fits')

                extra_logging_info = 'ramp #{}, frame {} of {}'.format(
                    self._sequence_num, frame_num+1,
                    self._num_output_frames)

                idx = (idx + 1) % int(self._data[keys.D_FILESPERSET])
                self._data[keys.D_SRCFILEINDEX] = int(idx)

                return SimFile(src_filename, tgt_filename, extra_logging_info)

            else:
                # Continue with making the frame and creating extra logging
                # info for FrameCount
                if 'FrameCount' == frame_gen_mode:
                    PBCamera.make_image(
                        frame_gen_mode, self._settings[keys.S_H], frame_count)

                    # extra_logging_info = '{}={}'.format(
                    #     self._data[keys.D_FRAMEGENMODE], frame_count)
                    extra_logging_info = '{}={} (16-bit pixel value=0x{:04x})'.format(
                        self._data[keys.D_FRAMEGENMODE], frame_count,
                        (frame_count & 0xFFFF))
                else:
                    extra_logging_info = self._frame_gen_info_str

                return FrameGenHdu(PBCamera.hdu, tgt_filename, extra_logging_info)
        #
        # End of ReadoutSequence.__prep_frame_data()


        #--------------------------- run() -------------------------------------
        #
        def run(self):
            with PBCamera.lk_exp_in_progress:
                PBCamera.exp_in_progress += 1

            logging.info("******** Starting Readout Sequence #%d - nOutputFrames=%d ********" %
                (self._sequence_num, self._num_output_frames))

            for readout in self._list_of_readouts:
                if const.DEBUG_READOUT_SEQUENCE:
                    debug_msg = 'type={}, action={}, target_time={}'.format(
                        readout.frame_type, readout.action, readout.target_time)
                    logging.debug(debug_msg)

                # Prep frame data and then wait for remainder of action timing
                if not 'NO_ACTION' == readout.action and self._mode in ('SFRAME','FFRAME'):
                    # Prep data for use following the next readout
                    frame_info = self.__prep_frame_data(
                        self._data[keys.D_FRAMEGENMODE],
                        self._data[keys.D_FRAMECOUNT])

                    # Add this set of src, tgt, log_info to the list
                    self._frames_to_write.append(frame_info)

                # Wait here for the target time simulating a frame read or
                # notified to stop.
                # NOTE: File prep above has consumed some of the frame read time.
                with self._cv_stop:
                    waitTime = readout.target_time - time.time()
                    stop_signaled = self._cv_stop.wait(waitTime)

                if stop_signaled:
                    logging.info('Active ReadoutSequence stopped!')
                    break

                info_msg = '({:>5}) Readout complete for sequence #{}, NDR #{} at {:.06f}'.format(
                    readout.frame_type, self._sequence_num, readout.frame_num,
                    readout.target_time)
                logging.info(info_msg)

                if 'NO_ACTION' == readout.action:
                    # Do nothing. The target time may have been a reset and the
                    # readout is discarded (Reset frame of SFRAME)
                    pass

                elif 'WRITE_FILE' == readout.action:
                    # A readout just completed. Get the next file from the
                    # frames_to_write list and send if to the file manager to be
                    # written. The file manager will indicate WRITTEN when the
                    # file write is complete.

                    info_msg = 'WRITING FILE sequence={}, file={}'.format(
                        self._sequence_num, readout.frame_num)
                    logging.info(info_msg)

                    frame_info = self._frames_to_write.pop(0)
                    resp_str = '{} {} WRITTEN'.format(
                        frame_info.tgt_filename, readout.frame_num-1)

                    frame_info.set_written_response(resp_str)
                    self._file_handler.write(frame_info)

                    # Adjust counts...
                    with PBCamera.lk_info:
                        PBCamera.info['ndr_count'] -= 1
                        PBCamera.info['readout_count'] += 1
                        PBCamera.info['written_count'] += 1
                    with PBCamera.lk_data:
                        # Increment frame counter in the camera and locally
                        PBCamera.data[keys.D_FRAMECOUNT] += 1   # camera level
                    self._data[keys.D_FRAMECOUNT] += 1    # this ReadoutSequence

            with PBCamera.lk_exp_in_progress:
                PBCamera.exp_in_progress -= 1

            # Signal via event that the readout sequence is complete which is
            # due to either full execution or because it was stopped.
            self._ev_complete.set()
        #
        # End of ReadoutSequence.run()
    #
    # End of class PBCamera.ReadoutSequence
#
# End of class PBCamera


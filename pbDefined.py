import numpy

###------------------------------ pbCommands -----------------------------------
class pbCommands:
  INIT     = "init"        # PB
  POF      = "pof"         # PB
  GO       = "go"          # PB
  RGO      = "rgo"         # Sim Only - 'go' with file index/framecount reset
  STOP     = "stop"        # PB
  SET      = "set"         # PB
  INFO     = "info"        # PB
  STATUS   = "status"      # PB
  OFFSET   = "offset"      # PB
  BEAGLE   = "beagle"      # PB
  SHUTDOWN = "shutdown"    # PB
  SIMTRIG  = "simtrig"     # Sim Only - Simulated HW trigger
  RSIMTRIG = "rsimtrig"    # Sim Only - Sim HW trig with file index/framecount reset
  DATA     = "data"        # Sim Only

###-------------------------------- pbKeys -------------------------------------
class pbKeys:
  SLOW    = 'slow'
  FAST    = 'fast'
  RSTRDRD = 'rstrdrd'

  # For use by the cmd line argparse to match key/value pairs with valid keys
  ALL_TYPES = [SLOW, FAST, RSTRDRD]
  SLOW_FAST = [SLOW, FAST]
  CMDLINE_ARGS = {'lat':   ALL_TYPES,
                  'rend':  ALL_TYPES,
                  'lt':    SLOW_FAST,
                  'fsync': SLOW_FAST,
                  'vclk':  SLOW_FAST}

  # Dictonary keys that are shared/used in both PBInterface and PBCamera for 
  # camera settings ('S_' denotes settings dictionary)
  S_DT    = 'dt'
  S_MODE  = 'mode'
  S_H     = 'h'
  S_Y     = 'y'
  S_NNDR  = 'nNDR'
  S_TRIG  = 'trigger'
  S_PATH  = 'path'
  S_FNAME = 'fileName'
  S_FNUM  = 'fileNumber'

  # Dictonary keys that are shared/used in both PBInterface and PBCamera for 
  # frame generatore/sim data settings ('D_' denotes data dictionary)
  D_FRAMEGENMODE    = 'frameGenMode'
  D_FIXEDPIXELVALUE = 'fixedPixelValue'
  D_PATH            = 'path'
  D_BASEFILENAME    = 'baseFilename'
  D_FILESPERSET     = 'filesPerSet'
  D_FRAMECOUNT      = 'frameCount'     # Counter used with "FrameCount" mode
  D_SRCFILEINDEX    = 'srcFileIndex'   # Used for indexing sim data files

###------------------------------ pbConstants ----------------------------------
class pbConstants:
  DEBUG_FILE_WRITER_THREAD = False
  DEBUG_READOUT_SEQUENCE = False

  BIT_TYPE = numpy.uint16       # Allowed bit depths and the numpy equivalents
  SENSOR_WIDTH  = 2048          # Sensor array width(x) in pixels
  SENSOR_HEIGHT = 2048          # Sensor array height(y) in pixels
  READ_CHANNELS = 32            # Number of readout channels per column
  MAX_FILES     = 100000        # Max num files before rolling back to 0
  MAX_SEQUENCES = 100000        # Max num sequences before rolling back to 0
  INIT_TIME = 22.5              # Actual time for init command to finish (sec)
  INIT_TIME = 1                 # Unrealistic shorter time for sim/debug
  MS_PER_SECOND = 1.0e3
  NS_PER_SECOND = 1.0e9
  START_OF_COMMENT = '#'
  RSTRDRD_FRAMES_PER_EXP = 4    # Multiplier for RSTRDRD NDR

  # Camera modes accepted by "set -m" (mode) command
  ALLOWED_MODES = ["SFRAME", "FFRAME", "RSTRDRD"]

  # The absolute min, used with 'set -r <val>'
  # Actual min cannot be checked until mode is set and 'go' is issued
  MIN_NDRS = {'SFRAME':  2, 'FFRAME':  3, 'RSTRDRD': 1}

  # Frame generator modes
  ALLOWED_FRAMEGEN_MODES = ["Off", "ColumnCount", "FixedPixel", "FrameCount", "RowCount"]


###------------------------------ pbDefaults -----------------------------------
class pbDefaults:
  # Default pbSim server host and port.
  SERVER_HOST = 'localhost'
  SERVER_PORT = 8000

  # Default path to ramdisk tmpfs used for temporary storage of fits files
  # created by pbSim and its integrated frame-generator."  
  # Configurable with 'set' command, --path argument.
  PATH_TO_TMPFS = '/tmp/nirsp_tmpfs/'
  FILENAME = 'pizza'
  FILE_NUMBER = 0
  SEQ_NUMBER = 1

  # Default path and file name for simulator logging
  LOG_FILE = '/tmp/cn_pbserver.log'

  MODE = pbConstants.ALLOWED_MODES[0]                    # Default camera mode

  FRAMEGEN_MODE = pbConstants.ALLOWED_FRAMEGEN_MODES[4]  # Default framegen mode
  FIXED_PIXEL_VALUE = 1024                               # Default fixed pixel value
  TRIGGER = 0                                            # Default is not HW triggers
  SUBARRAY_START_ROW = 0

  #========================= SENSOR/CONTROLLER TIMING

  # Default trigger latency times (in ns)
  T_LAT_TIMES = {pbKeys.SLOW: 240, pbKeys.FAST: 240, pbKeys.RSTRDRD: 240}

  # Default rend (T_USB) times (in ns)
  T_REND_TIMES = {pbKeys.SLOW: 0, pbKeys.FAST: 20000, pbKeys.RSTRDRD: 20000}

  # Default line times (in ns)
  T_LINE_TIMES = {pbKeys.SLOW: 243400, pbKeys.FAST: 44120}

  # Default frame sync times (in ns)
  T_FSYNC_TIMES = {pbKeys.SLOW: 620, pbKeys.FAST: 180}

  # Default vclk sync times (in ns)
  T_VCLK_TIMES = {pbKeys.SLOW: 500, pbKeys.FAST: 220}

  T_FRST = 1500        # T_fpga_reset (in ns)
  T_HRST = 320         # T_HRESETB (in ns)
  T_SDEL = 2090800     # T_reset_full_frame_linebyline (in ns)
  T_LRST = 2000        # T_lineReset (in ns)
  T_LFEND = 44520      # T_rstrdrd_extra (in ns)
  T_CRT = 0            # Unused extra (in ns)

  DT_S = 0.5           # The default exposure time dt(in sec)

  #========================= COMMAND TIMING
  #
  # Default wait time (seconds) to simulate the time it takes PBServer to return
  # from a SET command
  T_SET = 5.1

  # Default wait time (seconds) to simulate the time it takes PBServer to return
  # from a STOP command
  T_STOP = 7.2


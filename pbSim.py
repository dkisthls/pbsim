############################################################################################
# PBServer Simulator 2017-09-25 ho
# Initially Written by: Ian Cunnyngham (ian@cunnyngham.net)
# Maintained and Modified by: Chris Berst (cberst@nso.edu)
#
#   Attempts to mimic the behavior of the PBServer and attached camera for
# CSS testing and development purposes.
#
#   Developed against Python 3.6.0, defailt anaconda 4.3.25 environment
#   ( astropy (2.0.1) and numpy (1.13.1) are the only non-python libs if you don't want to use anacconda )
#
#  Running:
#
#    With the above described environment setup, just run
#  python pbSim.py
#
#    This will open a socket to listen on localhost port 8000.  You can telnet in 
#  telnet localhost 8000
#
#  ... or build up a programatic interface.  For some reason, upon initial client connection 
#  the server sends the client an "IARC\n".  After that, it is an interactive command line interface.
#  It only accepts one connection at a time, and when the client disconnects, it begins waiting for 
#  the next connection with no change in internal state.  For details on the command-line interface
#  see the PB1_Software_Interface document.  For now, the only output possible is .fits files. 
#  However, with various kinds of testing (including on the real controller) we have shown this to
#  be quite feasible on a ramdisk.  At some point a shared memory interface will be written, but
#  this doesn't exist in the actual controller software yet.
#
#  All output is logged to the console as well as a file: server.log
#
#    As mentioned above, I suggest you setup a tmpfs ramdisk for fits to be written to
#  sudo mount -t tmpfs -o size=1g tmpfs data_tmpfs
#  
# Potential shortcomings in this version
#   shared memory -  Need to see how Charles implements.
#   --trigger     -  Does nothing. Should potentially at least block go?
#   status        -  Always returns READY.  Again, need async implementation 
#                    for anything else to make sense
#   offset        -  Doesn't do anything besides return SUCCESS, not even validation.  
#                    Not sure what proper input looks like
#   - Timeings in frame mode are simplified a bit (total time split evenly between all frames)
#   - Error returns almost certainly don't match Charles'
#   - CDS and RSTRDRD mode returns are idealized because not currently fully implemented IRL
#
# ( Update 2017-09 )
#   - Stripped out set --well
#   - Renamed mode LBL -> RSTRDRD 
#   - Changed behavior of setting path to create non-existant target directories (as PBServer does)
#   - Need to move exposure time errors to set command
#   - Big refactor! Moved all camera settings into a dictionary to allow for easy copying before set
#                   And removed validateOnly section of set commands.  Now everything can be set
#                   and if there are any failures along the way, all will be reverted.  This was done
#                   to allow timing to be checked from the set command
#   - Added beagle command which doesn't do anything, but allows those to be passed in without error
#
# (Update 2017-10 )
#   - Added SHUTDOWN command at Chris's request
#
# (Update 2018-04 )
#   - Errors now prepend cmd numbers
#   - set -y and set -h fixed (also, they were crosswired)
#   - INFO return now a very ugly copy of PBServer as requested...
#   - Implemented "stop" interupt within the "go" command without threads
#
# (Update 2018-07-29 )
#   - Corrected return value in setNDR() from Flase --> False
#   - Modified formatted output in do_set(args.r). NDRs is an integer but output is using %f.
#     Output now formatted using the %i format specifier.
#
# (Update 2018-08-08)
#   - Per meeting: Added constant for minimum NDRs and changed minimum value from 1 --> 2.
#
###############################################################################################

### Library imports ###

import argparse              # Command line parser (used for set command)
import copy                  # For cloning settings dictionary
import logging, sys          # For log generation
import numpy as np           # For generating image array for fits file
import os                    # For checking directory exists / creating if not when setting path
import select                # For dealing with non-blocking sockets with a timeout (stop cmd)
import shutil                # For copying fits files
import socket                # For dealing with all things sockets
import threading
import time                  # For generating timing and sleep calls

from astropy.io import fits  # For saving fits data
from pathlib import Path     # For dealing with sim data paths/filenames

### PizzaBox Sim Imports ###
from pbCamera import PBCamera
from pbCamera import FrameGenHdu, SimFile
from pbDefined import pbCommands as pbcmd
from pbDefined import pbConstants as const
from pbDefined import pbDefaults as default
from pbDefined import pbKeys as keys

# Debug (can be commented out if not needed)
# from IPython.core.debugger import Tracer # For debugging in IPython

# Custom action that can be used by the argument parser to check whether a
# supplied value is >= 0.
# Note that geq 0 is equivalent to using HasLimitsAction where
# minium=0, maximum=None. This is just clearer (I think).
class GEQZeroAction(argparse.Action):
    #---------------------------- __call__() -----------------------------------
    #
    def __call__(self, parser, namespace, value, option_string=None):
        if not value >= 0:
            raise argparse.ArgumentError(self, "time value must be >= 0")
        setattr(namespace, self.dest, value)
    #
    # End of GEQZeroAction.__call__()


# Custom action that can be used by the argument parser to add an integer
# argument >= 0 to a dictionary. Use to process arguments of the form:
#     --arg KEY=VALUE [KEY=VALUE]...
class ParseIntDictGEQZeroAction(argparse.Action):
    #---------------------------- __call__() -----------------------------------
    #
    def __call__(self, parser, namespace, values, option_string=None):
        d = getattr(namespace, self.dest, {})

        if values:
            for item in values:
                split_items = item.split("=", 1)      # split key and value
                key = split_items[0].strip()          # keys are upper case
                value = int(split_items[1])           # convert to integer

                # Is this a valid key for the current argument?
                if not key in keys.CMDLINE_ARGS[self.dest]:
                    raise argparse.ArgumentError(self, 
                        "invalid key for argument, valid keys are {}".format(
                            keys.CMDLINE_ARGS[self.dest]))

                # Is value geq 0?
                if not value >= 0:
                    raise argparse.ArgumentError(self, 
                        "time value must be >= 0, {}={}".format(key, value))

                d[key] = value
        setattr(namespace, self.dest, d)
    #
    # End of ParseIntDictGEQZeroAction.__call__()


### Configuration ###

# Setup parser for command-line arguments
cmdLineParser = argparse.ArgumentParser(
    add_help=True,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

cmdLineParser.add_argument("--lat", dest="lat", 
    default=copy.deepcopy(default.T_LAT_TIMES),
    action=ParseIntDictGEQZeroAction, metavar="KEY=VALUE", nargs="+",
    help="Trigger Latency - Delay after timing card sends trigger(ns).")

cmdLineParser.add_argument("--rend", dest="rend", 
    default=copy.deepcopy(default.T_REND_TIMES),
    action=ParseIntDictGEQZeroAction, metavar="KEY=VALUE", nargs="+",
    help="T_USB (SFRAME) - Additional time to make sure USB is done transferring last pixel(ns).")

cmdLineParser.add_argument("--lt", dest="lt", 
    default=copy.deepcopy(default.T_LINE_TIMES),
    action=ParseIntDictGEQZeroAction, metavar="KEY=VALUE", nargs="+",
    help="Line times(ns).")

cmdLineParser.add_argument("--fsync", dest="fsync", 
    default=copy.deepcopy(default.T_FSYNC_TIMES),
    action=ParseIntDictGEQZeroAction, metavar="KEY=VALUE", nargs="+",
    help="Frame sync times(ns).")

cmdLineParser.add_argument("--vclk", dest="vclk", 
    default=copy.deepcopy(default.T_VCLK_TIMES),
    action=ParseIntDictGEQZeroAction, metavar="KEY=VALUE", nargs="+",
    help="Vertical clock times(ns).")

cmdLineParser.add_argument("--frst", type=int, 
    action=GEQZeroAction, dest="frst", default=default.T_FRST,
    help="T_fpga_reset - Time to reset all FPGAs to state 0(ns).")

cmdLineParser.add_argument("--hrst", type=int, 
    action=GEQZeroAction, dest="hrst", default=default.T_HRST,
    help="T_HRESETB - Row shift register reset time(ns).")

cmdLineParser.add_argument("--sdel", type=int, 
    action=GEQZeroAction, dest="sdel", default=default.T_SDEL,
    help="T_reset_full_frame_linebyline - Time to reset array line by line(ns).")

cmdLineParser.add_argument("--lrst", type=int, 
    action=GEQZeroAction, dest="lrst", default=default.T_LRST,
    help="T_lineReset - Time to reset length of line(ns).")

cmdLineParser.add_argument("--lfend", type=int, 
    action=GEQZeroAction, dest="lfend", default=default.T_LFEND,
    help="T_rstrdrd_extra - Extra time needed at the end of a frame(ns).")

cmdLineParser.add_argument("--crt", type=int, 
    action=GEQZeroAction, dest="crt", default=default.T_CRT,
    help="Defined but unused time value(ns).")

cmdLineParser.add_argument("--tcmd_set", type=float, 
    action=GEQZeroAction, dest="tCmdSet", default=default.T_SET,
    help="Amount of time, in seconds, a set command takes on pbServer.")

cmdLineParser.add_argument("--tcmd_stop", type=float, 
    action=GEQZeroAction, dest="tCmdStop", default=default.T_STOP,
    help="Amount of time, in seconds, a stop command takes on pbServer.")

cmdLineParser.add_argument("--logfile", type=str,
    action="store", dest="logfile", default=default.LOG_FILE,
    help="Sets path and filename for simulator logging.")

cmdLineParser.add_argument("--port", type=int,
    action="store", dest="port", default=default.SERVER_PORT,
    help="Sets port number for simulator cmd connection.")

# Setup parser for the data sub-command
dataParser = argparse.ArgumentParser(add_help=False)

dataParser.add_argument("--frameGenMode", type=str, 
                        choices=const.ALLOWED_FRAMEGEN_MODES,
                        help="sets framegen mode")
dataParser.add_argument("--fixedPixelValue", type=int,
                        help="sets framegen fixed pixel value")
dataParser.add_argument("--path", type=str,
                        help="sets path to sim data files")
dataParser.add_argument("--baseFilename", type=str,
                        help="sets base filename for sim data files")
dataParser.add_argument("--filesPerSet", type=int,
                        help="sets the number of files in the named set")

# Setup parser for the set sub-command
setParser = argparse.ArgumentParser(add_help=False)

setParser.add_argument('-m', type=str.upper,
                       help="sets camera mode")
setParser.add_argument('--trigger', type=int,
                       help="sets trigger (1 set, 0 unset)")
setParser.add_argument('-t', type=float,
                       help="exposure time in seconds")
setParser.add_argument('-r', type=int,
                       help="number of NDRs")
setParser.add_argument('-y', type=int, 
                       help="starting row in sub-array read")
setParser.add_argument('-h', type=int,
                       help="number of rows in sub-array read")
setParser.add_argument('-f', type=str,
                       help="base of FITS filename: e.g. 'foo' -> 'path/foo-##-####.fits'")
setParser.add_argument('--path', type=str,
                       help="path to write FITS files")
setParser.add_argument('-n', type=int,
                       help="manually set file number from 0 - "+str(const.MAX_FILES - 1))

### Class definitions ###

class PBSimExposure(threading.Thread):
    #---------------------------- __init__() -----------------------------------
    # pbinterface - The pbInterface (cmd interface) object
    # pbcam - The pbCamera object
    # socket_handler - cmd/resp socket
    # trigger_time - Time either 'go' or 'simtrig' was received
    # is_hw_trigger - Reflects current value of settings['trigger']
    def __init__(self, pbinterface, pbcam, trigger_time, is_hw_trigger):
        threading.Thread.__init__(self)

        self._pbinterface = pbinterface
        self._pbcam = pbcam
        self._trigger_time = trigger_time
        self._is_hw_trigger = is_hw_trigger
        self._forced_stop = False
    #
    # End of PBSimExposure.__init__()


    #------------------------------- run() -------------------------------------
    # threading.Thread.run() overload
    #
    def run(self):
        if not self._forced_stop:
            # Start the exposure thread - this will return quickly
            (ev_exposure_complete, t_total_ramp_time) = \
                self._pbcam.execute_exposure(self._trigger_time)

        if not self._forced_stop:
             # Wait for the entire exposure, started above, to complete.
            ev_exposure_complete.wait(t_total_ramp_time + 0.5)

        # If Software triggering and not forced stop indicate completion now.
        if not self._is_hw_trigger and not self._forced_stop:
            self._pbinterface.send_response_nn_msg("SUCCESS")
            self._pbcam.set_info_value('controller_go', 0)
    #
    # End of PBSimExposure.run()


    #------------------------------ stop() -------------------------------------
    #
    def stop(self):
        self._forced_stop = True    # Assert flag for our run method
        self._pbcam.stop()          # Stop the exposure in progress
    #
    # End of PBSimExposure.stop()
#
# End of class PBSimExposure


class PBInterface:
    #---------------------------- __init__() -----------------------------------
    #
    def __init__(self, cmd_line_args, socket_handler):
        # Dictionary of PizzaBox commands(keys) and their associated methods
        self._command_dict = {
            pbcmd.INIT: self.do_init,         # PB
            pbcmd.POF: self.do_pof,           # PB
            pbcmd.GO: self.do_go,             # PB
            pbcmd.RGO: self.do_rgo,           # Sim Only, 'go' with file index/framecount reset
            pbcmd.STOP: self.do_stop,         # PB
            pbcmd.SET: self.do_set,           # PB
            pbcmd.INFO: self.do_info,         # PB
            pbcmd.STATUS: self.do_status,     # PB
            pbcmd.OFFSET: self.do_offset,     # PB
            pbcmd.BEAGLE: self.do_beagle,     # PB
            pbcmd.SHUTDOWN: self.shutdown,    # PB
            pbcmd.SIMTRIG: self.do_simtrig,   # Sim Only - Simulated HW trigger
            pbcmd.RSIMTRIG: self.do_rsimtrig, # Sim Only - Sim HW trig with file index/framecount reset
            pbcmd.DATA: self.do_data          # Sim Only
        }

        # Create an instance of our FileHandler
        try:
            self._file_handler = FileHandler(self)
        except RuntimeError as e:
            raise e

        self._cla = cmd_line_args             # Save command line arguments
        self._cmd_argv = []
        self._cmdNumber = -1                   # Keeps track of current cmd number
        self._pbcam = PBCamera(cmd_line_args, socket_handler, self._file_handler)
        self._socket_handler = socket_handler
        self._sim_trig_active = False;
        self._t_reset_ns = None              # Calculated time for reset frame (in ns)
        self._t_frame_read_ns = None         # Calcualted time for NDR frame read (in ns)
        self._t_total_ramp_time_ns = None    # Calcualted total ramp time (in ns)

        # Used with software triggering (--trigger 0)
        # to capture the PBSimExposure object when a 'go' command is received
        # and then, if a 'stop' command is received, come back to it to stop the
        # exposure in progress.
        self._active_exposure = None
    #
    # End of PBInterface.__init__()


    #------------------------ validate_settings() ------------------------------
    # Utility method used to validate settings that may have been configured
    # with separate 'set' commands. On receipt of a 'go' command, this mehthod
    # should be called to perform the final 'settings' validation.
    #
    def _validate_settings(self):
        success = True
        sts_msg = 'OK'

        # Snapshot current settings and data before proceeding.
        settings = self._pbcam.get_settings()
        mode = settings[keys.S_MODE]
        nNDR = settings[keys.S_NNDR]
        y = settings[keys.S_Y]
        h = settings[keys.S_H]

        # mode and nNDR may have been set with separate 'set' commands.
        # Check that the defined number of NDRs is >= minimum for current mode
        if nNDR < const.MIN_NDRS[mode]:
            logging.info(
                'Invalid number of NDRs for %s: minimum=%d, NDRs=%d! Aborting...' %
                (mode, const.MIN_NDRS[mode], nNDR) )
            success = False
            sts_msg = 'ERROR: Bad NDR setting'

        # y and h may have been set with separate 'set' commands.
        # Check that y+h is within limits of sensor height.
        elif (y + h) > const.SENSOR_HEIGHT:
            logging.info(
                'Bad combination of subarray start and height: y=%d, h=%d! Aborting...'%
                (y, h) )
            success = False
            sts_msg = 'ERROR: Bad sub-array specification'

        return (success, sts_msg)
    #
    # End of PBInterface._validate_settings()


    #----------------------------- do_init() -----------------------------------
    #
    def do_init(self):
        self.send_response_ok_nn_cmd('init')      # 'OK NN init'
        self._pbcam.init()
        self.send_response_nn_msg("SUCCESS")
        self._pbcam.inc_info_value('init_count')    # increment init counter
    #
    # End of PBInterface.do_init()


    #----------------------------- do_pof() ------------------------------------
    #
    def do_pof(self):
        self._pbcam.pof()
        self._socket_handler.send("Powering OFF Controller")
    #
    # End of PBInterface.do_pof()

    
    #------------------------------ do_go() ------------------------------------
    #
    def do_go(self):
        if self._pbcam.is_initialized():

            go_received_at = time.time()    # Mark the 'trigger' time

            (valid, sts_msg) = self._validate_settings()

            if valid:
                self._pbcam.set_info_value('controller_go', 1)
                self.send_response_ok_nn_cmd('go')        # 'OK NN go'

                # If using SW triggers, simulate ramp now...
                settings = self._pbcam.get_settings()
                if not settings[keys.S_TRIG]:
                    self._active_exposure = PBSimExposure(
                        self, self._pbcam, go_received_at, settings[keys.S_TRIG])

                    self._active_exposure.start()
                else:
                    # For clarity, clear the SW Trigger Go object
                    self._active_exposure = None

            else:
                self.send_response_nn_msg(stsMsg)
        else:
            logging.info("Array powered off, doing nothing")
            self.send_response_nn_msg("ERROR: Array powered off")
    #
    # End of PBInterface.do_go()


    #----------------------------- do_rgo() ------------------------------------
    #
    def do_rgo(self):
        # Reset source file index to 0 then execute 'go'
        self._pbcam.data[keys.D_SRCFILEINDEX] = int(0)
        self.do_go()
    #
    # End of PBInterface.do_rgo()


    #----------------------------- do_stop() ----------------------------------
    #
    def do_stop(self):
        if not self._pbcam.get_info_value('controller_go'):
            self.send_response_nn_msg(
                'ERROR: --type STATUS --msg STOP FAILED, CONTROLLER IN READY STATE')
        else:
            # Respond immediately with 'OK' response.
            # If an exposure is active, simulate PBServer stop time and cleanup.
            self.send_response_ok_nn_cmd('stop')      # 'OK NN stop'

            settings = self._pbcam.get_settings()
            if self._pbcam.get_num_exp_in_progress() == 0:    # Anything active?
                # No. Just respond with 'SUCCESS'
                self.send_response_nn_msg("SUCCESS")

                if settings[keys.S_TRIG]:
                    # This completes the active 'go' command under HW triggering.
                    # The command number may now be incremented.
                    self._cmdNumber += 1

            elif settings[keys.S_TRIG]:                       # HW trigger mode?
                # Stop the active exposure then simulate PBServer stop time
                self._pbcam.stop()
                time.sleep(self._cla.tCmdStop)
                self.send_response_nn_msg("SUCCESS")

                # This completes the active 'go' command under HW triggering.
                # The command number may now be incremented.
                self._cmdNumber += 1
                self._sim_trig_active = False

            else:                                             # SW trigger mode
                # Stop the active 'go' command then simulate PBServer stop time
                self._active_exposure.stop()
                time.sleep(self._cla.tCmdStop)
                self.send_response_nn_msg("SUCCESS")

            # The active 'go' can now be reset
            self._pbcam.set_info_value('controller_go', 0)
    #
    # End of PBInterface.do_stop()


    #--------------------------- do_simtrig() ----------------------------------
    #
    def do_simtrig(self):
        # A 'simtrig' command serves as a "soft" hardware trigger. It is only valid
        # when 'set --trigger=1' and following receipt of a 'go' command. 
        # If a 'go' is active, we're in HW trigger mode, and a trigger is not already
        # active, execute a ramp. 
        # Othewise, log the variables for a simtrig and then ignore the command.

        simtrig_received_at = time.time()

        settings = self._pbcam.get_settings()

        if self._pbcam.get_info_value('controller_go') and\
           settings[keys.S_TRIG] and not self._sim_trig_active:
            self._sim_trig_active = True

            # Simulate exposure now...
            self._active_exposure = PBSimExposure(
                self, self._pbcam, simtrig_received_at, settings[keys.S_TRIG])
            self._active_exposure.start()

            self._sim_trig_active = False
        else:
            logging.info("IGNORING 'simtrig': pbcam.info.controller_go=%d, "\
                         "pbcam.settings.trigger=%d, simTrigActive=%d"%\
                (self._pbcam.get_info_value('controller_go'), settings[keys.S_TRIG], self._sim_trig_active))
    #
    # End of PBInterface.do_simtrig()


    #--------------------------- do_rsimtrig() ---------------------------------
    #
    def do_rsimtrig(self):
        # This command is used to reset source file index to 0 then execute 'simtrig'
        # Reset source file index to 0 then execute 'go'
        self._pbcam.data[keys.D_SRCFILEINDEX] = int(0)
        self.do_simtrig()
    #
    # End of PBInterface.do_rsimtrig()


    #----------------------------- do_data() ----------------------------------
    #
    def do_data(self):
        self.send_response_ok_nn_cmd('data')      # 'OK NN data'
        
        # Check to make sure arguments were passed
        if len(self._cmd_argv) == 0:
            logging.info("No data options specified, doing nothing")
            self.send_response_nn_msg("ERROR: No data options specified")
            return
        
        # Run arguments through argparse instance we set up at beginning
        # It does basic validation, so catch any errors it throws up
        # ( More detailed errors go to stdout, user gets general error )
        try:
            args = dataParser.parse_args(self._cmd_argv)
        except SystemExit:
            logging.info("Bad data options")
            self.send_response_nn_msg("ERROR: Bad data options")
            return

        # Snapshot current data before proceeding.
        # get_data() returns a deepcopy.
        curr_data = self._pbcam.get_data()
        
        # Validate all data command arguments that are present
        data_error = False        # Track if any data errors occured
        data_results = []        # Track data command changes for logging
        new_data = {}            # Dictionary for new data parameters

        #--------------------------  data --frameGenMode
        #
        if args.frameGenMode != None:
            if args.frameGenMode in const.ALLOWED_FRAMEGEN_MODES:
                new_data[keys.D_FRAMEGENMODE] = args.frameGenMode
                data_results.append( str("frameGen mode %s"%(args.frameGenMode)) )
            else:
                logging.info("Bad frameGen mode value")
                self.send_response_nn_msg(
                    "ERROR: Invalid frameGenMode=%s"%(args.frameGenMode))
                data_error = True
        
        #--------------------------  data --fixedPixelValue
        #
        if not data_error and args.fixedPixelValue != None:
            if args.fixedPixelValue >= 0:
                new_data[keys.D_FIXEDPIXELVALUE] = args.fixedPixelValue
                data_results.append( str("frameGen fixedPixelValue: %d"%(args.fixedPixelValue)) )
            else:
                logging.info("fixedPixelValue must be >= 0")
                self.send_response_nn_msg(
                    "ERROR: Bad fixedPixelValue=%d, value must be >= 0"%(args.fixedPixelValue) )
                data_error = True

        #--------------------------  data --path
        #
        if not data_error and args.path != None:
            # Check to make sure this is a legitamate path, signal failure if not.
            path = Path(args.path)
            if path.exists():
                new_data[keys.D_PATH] = path
                data_results.append( str("sim data path: '%s'"%(args.path)) )
            else:
                logging.info("sim data path '%s' does not exist"%(args.path))
                self.send_response_nn_msg(
                    "ERROR: Bad path, '%s' does not exist"%(args.path))
                data_error = True

        #--------------------------  data --baseFilename
        #
        if not data_error and args.baseFilename != None:
            new_data[keys.D_BASEFILENAME] = args.baseFilename
            data_results.append( str("base filename: %s"%(args.baseFilename)) )

        #--------------------------  data --filesPerSet
        #
        if not data_error and args.filesPerSet != None:
            if args.filesPerSet > 0:
                new_data[keys.D_FILESPERSET] = args.filesPerSet
                data_results.append( str("files-per-set: %d"%(args.filesPerSet)) )
            else:
                logging.info("filesPerSet must be > 0")
                self.send_response_nn_msg(
                    "ERROR: Bad filesPerSet=%d, value must be > 0"%(args.filesPerSet) )
                data_error = True

        if data_error:
            return

        # Pick new frameGenMode (if present) otherwise use current value..
        frameGenMode = (new_data[keys.D_FRAMEGENMODE] \
            if keys.D_FRAMEGENMODE in new_data else curr_data[keys.D_FRAMEGENMODE])

        # Do path/filename checks if the mode will be 'Off'...
        if frameGenMode == "Off":
            success = False

            path = (new_data[keys.D_PATH] \
                if keys.D_PATH in new_data else curr_data[keys.D_PATH])
            baseFilename = (new_data[keys.D_BASEFILENAME] \
                if keys.D_BASEFILENAME in new_data else curr_data[keys.D_BASEFILENAME])
            filesPerSet = (new_data[keys.D_FILESPERSET] \
                if keys.D_FILESPERSET in new_data else curr_data[keys.D_FILESPERSET])

            if not path:
                errorReason = "sim data 'path' is not defined"
            elif not baseFilename:
                errorReason = "sim data 'baseFilename' is not defined"
            elif filesPerSet <= 0:
                errorReason = "sim data 'filesPerSet' is not defined"
            else:
                success = True

            if not success:
                self.send_response_nn_msg(
                    "ERROR: frameGenMode='Off' but %s" % (errorReason) )
                data_error = True
            else:
                # Build up filenames and test if file exists and is readable...
                for i in range(filesPerSet): 
                    filename = PBCamera._build_filename(baseFilename, i, "fits")
                    p = Path(path) / filename

                    if not p.exists():
                        self.send_response_nn_msg(
                            "ERROR: frameGenMode='Off' but '{}' does not exist".format(p) )
                        data_error = True
                        break

                    if not os.access(str(p), os.R_OK):
                        self.send_response_nn_msg(
                            "ERROR: frameGenMode='Off' but '{}' is not readable".format(p) )
                        data_error = True
                        break
       
        # If there are any data errors return without applying new values
        if data_error:
            return

        # All is good, apply the new data values...
        self._pbcam.update_data(new_data)

        logging.info( "data changes: "+", ".join(data_results) )

        self.send_response_nn_msg("SUCCESS")
    #
    # End of PBInterface.do_data()


    #----------------------------- do_set() ------------------------------------
    #
    def do_set(self):
        self.send_response_nn_msg("OK")
        
        # Check to make sure arguments were passed
        if len(self._cmd_argv) == 0:
            logging.info("No set options specified, doing nothing")
            self.send_response_nn_msg("ERROR: No set options specified")
            return
        
        # Run arguments through argparse instance we set up at beginning
        # It does basic validation, so catch any errors it throws up
        # ( More detailed errors go to stdout, user gets general error )
        try:
            args = setParser.parse_args(self._cmd_argv)
        except SystemExit:
            logging.info("Bad set options")
            self.send_response_nn_msg("ERROR: Bad set options")
            return
        
        # Snapshot current settings before proceeding.
        # get_settings() returns a deepcopy.
        curr_settings = self._pbcam.get_settings()

        # Validate all set command arguments that are present
        set_error = False        # Track if any set errors occured
        set_results = []         # Track set command changes for logging
        new_settings = {}        # Dictionary for new settings

        #--------------------------  set -m (mode)
        #
        if args.m != None:
            # Check that supplied mode is "on the list"
            if args.m in const.ALLOWED_MODES:
                new_settings[keys.S_MODE] = args.m
                set_results.append( str("mode %s" % args.m) )
            else:
                logging.info("Bad mode selection")
                self.send_response_nn_msg("ERROR: Problem setting mode")
                set_error = True

        #--------------------------  set -y -h  (start row, num rows)
        #
        if not set_error and (args.y != None or args.h != None):
            # NOTE: y+h is checked again on receipt of a 'go' command
            y = curr_settings[keys.S_Y]
            h = curr_settings[keys.S_H]

            argError = False
            if args.y != None:
                if args.y >= 0 and args.y < const.SENSOR_HEIGHT:
                    y = args.y
                    set_results.append( str("sub-array start: %i" % (y)) )
                else:
                    logging.info("Bad subarray y start")
                    argError = True

            if args.h != None:
                if args.h >= 1 and args.h <= const.SENSOR_HEIGHT:
                    h = args.h
                    set_results.append( str("sub-array size: %i" % (h)) )
                else:
                    logging.info("Bad subarray y size")
                    argError = True

            # If both h and y were specified, check for y+h > SENSOR_HEIGHT
            if not argError and args.y != None and args.h != None and y+h > const.SENSOR_HEIGHT:
                logging.info("Bad combination of subarray start and height: y=%d, h=%d! Aborting..."%
                             (y,h) )
                set_error = True

            if not argError:
                new_settings[keys.S_Y] = y
                new_settings[keys.S_H] = h
            else:
                self.send_response_nn_msg("ERROR: Bad sub-array specification")
                set_error = True

        #--------------------------  set -r (num NDRs)
        #
        if not set_error and args.r != None:
            # NOTE: mode/nNdr combo is checked on receipt of a 'go' command
            if args.r > 0:
                new_settings[keys.S_NNDR] = args.r
                set_results.append( str("# NDRs: %i" % args.r) )
            else: 
                logging.info("Bad NDR set number (<1)")
                self.send_response_nn_msg("ERROR: Bad NDR setting")
                set_error = True

        #--------------------------  set -t (inter-frame delay)
        #
        if not set_error and args.t != None:
            if args.t >= 0.0:
                new_settings[keys.S_DT] = args.t
                set_results.append( str("exposure time: %.9f (sec)" % args.t) )
            else:
                logging.info("Negative time supplied for dt...")
                self.send_response_nn_msg("ERROR: Bad exposure time")
                set_error = True

        #--------------------------  set --path
        #
        if not set_error and args.path != None:
            # Check to make sure this is a legitamate path, signal failure if not.
            if Path(args.path).exists():
                path = args.path
                if path[-1] != '/':          # Make sure the path ends in a '/'
                    path += '/'
                new_settings[keys.S_PATH] = path
                set_results.append( str("path: %s" % path) )
            else:
                logging.info(
                    "Directory '{}' does not exist, please create as a 1G tmpfs ramdisk!".format(args.path))
                self.send_response_nn_msg("ERROR: Bad path specified")
                set_error = True

        #--------------------------  set -f (filename)
        #
        if not set_error and args.f != None:
            new_settings[keys.S_FNAME] = args.f
            set_results.append( str("filename: %s" % args.f) )

        #--------------------------  set -n (file number)
        #
        if not set_error and args.n != None:
            if args.n >= 0 and args.n < const.MAX_FILES:
                new_settings[keys.S_FNUM] = args.n
                set_results.append( str("file number: %04d" % (args.n) ) )
            else:
                logging.info("Bad file number specified")
                self.send_response_nn_msg("ERROR: Bad file number specified")
                set_error = True

        #--------------------------  set --trigger (enb external trigger)
        #
        if not set_error and args.trigger != None:
            if args.trigger == 0 or args.trigger == 1:
                new_settings[keys.S_TRIG] = args.trigger
                set_results.append( str("trigger: %d" % (args.trigger) ) )
            else:
                logging.info("Bad trigger specified")
                self.send_response_nn_msg("ERROR: Bad trigger specified")
                set_error = True

        # If there are any set errors return without applying new settings
        if set_error:
            return

        # All is good, apply the new settings...
        self._pbcam.update_settings(new_settings)

        # Simulate how long it takes for PBServer to do a set.
        time.sleep(self._cla.tCmdSet)
        
        logging.info( "Set changes: "+", ".join(set_results) )

        self.send_response_nn_msg("SUCCESS")
    #
    # End of PBInterface.do_set()
    

    #----------------------------- do_info() -----------------------------------
    #
    def do_info(self):
        # The real PB Server injects a delay between sending "INFO" and sending the
        # remainder of the strings for the entire 'info' command. We do the same here.
        cur_info = str(self._pbcam)
        logging.info('\'info\' response:\nINFO\n%s' % cur_info)

        # First word of INFO response is 'INFO'
        self._socket_handler.send("INFO")

        # Delay between INFO and remainder. This sleep time results in an overall 
        # approximate info comand transmit/processing time of 170-180ms which mimics
        # that of PB server.
        time.sleep(0.04)

        # Send the rest of the strings
        self._socket_handler.send(cur_info)
    #
    # End of PBInterface.do_info()
        

    #---------------------------- do_status() ----------------------------------
    #
    def do_status(self):
        self.send_response_ok_nn_cmd('status')      # 'OK NN status'
        if self._pbcam.is_initialized():
            self.send_response_nn_msg("READY")
        else:
            self.send_response_nn_msg("UNKNOWN")
        self.send_response_nn_msg("SUCCESS")
    #
    # End of PBInterface.do_status()
        

    #---------------------------- do_offset() ----------------------------------
    #
    def do_offset(self):
        logging.info("Offset received, doing nothing with it")
        self.send_response_nn_msg("SUCCESS")
    #
    # End of PBInterface.do_offset()
    

    #---------------------------- do_beagle() ----------------------------------
    #
    def do_beagle(self):
        logging.info("beagle command received, doing nothing")
        # beagle commands don't return anything on the socket
    #
    # End of PBInterface.do_beagle()
    

    #--------------------- send_response_ok_nn_cmd() --------------------------
    # Formats and sends a response of the form:
    #    'OK <cmdNumber> <cmd>'
    #
    # The <cmdNumber> is maintained in the PBInterface class.
    # The <cmd> is a text string containing the command and is context dependent
    # from the caller.
    #
    # Examples:
    #    'OK 0 init', 'OK 12 go', 'OK 23 stop', 'OK 34 data', 'OK 45 status'
    #
    def send_response_ok_nn_cmd(self, cmd):
        # Format response with command number and supplied command string
        msg = str("OK %d %s" % (self._cmdNumber, cmd))

        self._socket_handler.send(msg)
        logging.info(msg)
    #
    # End of PBInterface.send_response_ok_nn_cmd()


    #---------------------- send_response_nn_msg() -----------------------------
    # Formats and sends a response of the form:
    #     '<cmdNumber> <msg>'
    #
    # The <cmdNumber> is maintained in the PBInterface class.
    # The <msg> is a text string and is context dependent from the caller.
    #
    # Examples:
    #     '3 SUCCESS', '5 OK', '25 ERROR: <error message>'
    #
    def send_response_nn_msg(self, msg):
        # Tack command number onto message
        msg = str("%d %s" % (self._cmdNumber, msg))

        self._socket_handler.send(msg)
        logging.info(msg)
    #
    # End of PBInterface.send_response_nn_msg()


    #---------------------------- shutdown() -----------------------------------
    #
    def shutdown(self):
        logging.info("Waiting for client disconnect...")
        self.send_response_nn_msg("SUCCESS")
        
        # Go into a readout loop, but respond to any request with an error
        # shutdown PBServer on client disconnect.
        done = False
        while not done:
            try:
                # Wait for a command
                sockMsg = self._socket_handler.recv()

                logging.info(
                    "Command received while waiting for shutdown, sending error")
                self.send_response_nn_msg(
                    "ERROR: PBServer is shutting down, disconnect to complete")
            except RuntimeError as e:
                if e.args[0] == "SOCK_BROKEN":
                    logging.info("Socket broken, shutting down...")
                    done = True
                else:
                    raise e
    #
    # End of PBInterface.shutdown()
    

    #---------------------------- parseCMD() -----------------------------------
    #
    def parseCMD(self, cmd_string):
        cmd_name = None     # The command to run
        cmd_func = None     # The function associated with the command

        # First strip away possible comment
        where = cmd_string.find(const.START_OF_COMMENT)
        if where != -1:
            cmd_string = cmd_string[:where]

        cmdSplit = cmd_string.split()
        if len(cmdSplit) > 0:
            cmd_name = str.lower(cmdSplit[0])
            self._cmd_argv = cmdSplit[1:]
            logging.info("RECEIVED: %s" % cmd_name)

            if cmd_name in self._command_dict:
                cmd_func = self._command_dict.get(cmd_name)

                # Only increment if a 'go' is not active and we're not using HW triggers
#                if not (self._pbcam.get_info_value('controller_go') and self._pbcam.settings[keys.S_TRIG]):
                if not self._pbcam.get_info_value('controller_go'):
                    self._cmdNumber += 1
            else:
                errMsg = 'Unrecongnized command \'{}\'!'.format(cmd_name)
                logging.info(errMsg)
                self.send_response_nn_msg('ERROR: ' + errMsg)

        return (cmd_name, cmd_func)
    #
    # End of PBInterface.parseCMD()
#
# End of class PBInterface


class FileHandler:
    #---------------------------- __init__() -----------------------------------
    #
    def __init__(self, pbinterface):
        self._pbinterface = pbinterface  # For access to send response strings
    #
    # End of FileHandler.__init__()


    #------------------------------ write() ------------------------------------
    #
    def write(self, item_to_write):
        isFile = isinstance(item_to_write, SimFile)
        isHdu  = isinstance(item_to_write, FrameGenHdu)

        if const.DEBUG_FILE_WRITER_THREAD: tWriteStart = time.time()

        # Do the write, either with an HDU from the frame-generator (isHdu)
        # or a sim file (isFile)
        if isHdu:
            info_str = 'Writing HDU for {} to file...\n{:>39} {}'.format(
                item_to_write.extra_log_info,
                'Target File:', item_to_write.tgt_filename)
            logging.info( info_str )  # Log the info string

            # Write data...
            item_to_write.hdu.writeto(item_to_write.tgt_filename, overwrite=True)

        else:  # isFile
            info_str = 'Copying sim file for {}...\n{:>39} {}\n{:>39} {}'.format(
                item_to_write.extra_log_info,
                'Source File:', item_to_write.src_filename,
                'Target File:', item_to_write.tgt_filename)
            logging.info( info_str )  # Log the info string

            # Write data...
            shutil.copyfile(item_to_write.src_filename, item_to_write.tgt_filename)

        if not item_to_write.resp_str is None:
            # Since we just wrote the file, issue the 'WRITTEN' response
            # string privided with the file/hdu.
            self._pbinterface.send_response_nn_msg( item_to_write.resp_str )

        if const.DEBUG_FILE_WRITER_THREAD:
            logging.info("Time to write fits: %.03fms"%\
                ((time.time() - tWriteStart) * const.MS_PER_SECOND))
    #
    # End of FileHandler.write()
#
# End of class FileHandler


class SocketHandler:
    #---------------------------- __init__() -----------------------------------
    #
    def __init__(self, host, port, cmd_line_args):

        # Create a socket of the standard TCP streaming type
        self.__sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Bind to host:port
        self.__sock.bind((host, cmd_line_args.port))

        # Set max connections to 1 and listen
        self.__sock.listen(1)
    #
    # End of SocketHandler.__init__()


    #------------------------- waitForConnect() --------------------------------
    #
    def waitForConnect(self):
        # Wait for connection
        (self.connection, self.address) = self.__sock.accept()
        logging.info("Received new connection on socket")
        
        # Need to do an initial send to the client (as PBServer does)
        self.send('IARC') # for some reason?
    #
    # End of SocketHandler.waitForConnect()


    #------------------------------ send() -------------------------------------
    #
    def send(self, msg):
        # Tack newline onto messages
        msg = str("%s\n" % (msg))
        
        # Encode the message and send
        sent = self.connection.send(str.encode(msg))
        
        if sent == 0:
            raise RuntimeError("SOCK_BROKEN")
    #
    # End of SocketHandler.send()


    #------------------------------ recv() -------------------------------------
    #
    def recv(self):
        # Wait for something to be received on this socket
        data = self.connection.recv(4098)
        
        # Catch socket broken with empty byte string
        if data == b'':
            raise RuntimeError("SOCK_BROKEN")
        
        return bytes.decode(data)
    #
    # End of SocketHandler.recv()
#
# End of class SocketHandler

    
### Main program runtime ###

# Parse command line arguments
cmd_line_args = cmdLineParser.parse_args()

# Set up our logs (These need to be initialized before we define our classes below)
logging.basicConfig(filename=cmd_line_args.logfile,
                    format='[%(asctime)s.%(msecs)03dZ] %(message)s', 
                    datefmt='%Y-%m-%dT%H:%M:%S',
                    level=logging.DEBUG)
logger = logging.getLogger()
logging.Formatter(fmt='[%(asctime)s.%(msecs)03d] %(message)s', datefmt='%Y-%m-%dT%H:%M:%S')
logging.Formatter.converter = time.gmtime

# Also output to console for now
consoleOut = logging.StreamHandler(sys.stdout)
consoleOut.setLevel(logging.INFO)

# Output that we're getting started
logging.info("Starting new pbSim run")
        
# Log the values being used that came either from the default values established
# with the command line parser or from the actual command line...
logging.info("Using lat (Trigger Latency) times(ns)         : {}".format(cmd_line_args.lat))
logging.info("Using rend (T_USB) times(ns)                  : {}".format(cmd_line_args.rend))
logging.info("Using lt (Line Readout) times(ns)             : {}".format(cmd_line_args.lt))
logging.info("Using fsync (Frame Sync) times(ns)            : {}".format(cmd_line_args.fsync))
logging.info("Using vclk (Vertical Clock) times(ns)         : {}".format(cmd_line_args.vclk))
logging.info("Using frst (FPGA reset) time(ns)              : {}".format(cmd_line_args.frst))
logging.info("Using hrst (Row shift register reset) time(ns): {}".format(cmd_line_args.hrst))
logging.info("Using sdel (Reset array line-by-line) time(ns): {}".format(cmd_line_args.sdel))
logging.info("Using lrst (Reset length of line) time(ns)    : {}".format(cmd_line_args.lrst))
logging.info("Using lfend (RSTRDRD extra) time(ns)          : {}".format(cmd_line_args.lfend))
logging.info("Using crt (UNUSED extra) time(ns)             : {}".format(cmd_line_args.crt))
logging.info("Using 'set' command time(s)                   : {}".format(cmd_line_args.tCmdSet))
logging.info("Using 'stop' command time(s)                  : {}".format(cmd_line_args.tCmdStop))
logging.info("Using logfile                                 : {}".format(cmd_line_args.logfile))
logging.info("Using port #                                  : {}".format(cmd_line_args.port))

# Create an instanace of our socket connection handler
socket_handler = SocketHandler(default.SERVER_HOST,\
                               default.SERVER_PORT, cmd_line_args)
        
# Create an instance of the camera command interface
pbint = PBInterface(cmd_line_args, socket_handler)

socket_handler.waitForConnect()

shutdown = False
while not shutdown:
    try:
        # Wait for a command
        sockMsg = socket_handler.recv()

        # Pass received string to PBInterface command parser which will
        # return the command name and associated processing function.
        (cmd, cmd_func) = pbint.parseCMD(sockMsg)

        if not cmd is None and not cmd_func is None:
            cmd_func()

            if cmd == pbcmd.SHUTDOWN:
                shutdown = True

    except RuntimeError as e:
        if e.args[0] == "SOCK_BROKEN":
            logging.info("Socket broken, waiting for reconnect...")
            socket_handler.waitForConnect()
        elif not shutdown:
            raise e
       

logging.info('pbSim Shutdown Complete!')

